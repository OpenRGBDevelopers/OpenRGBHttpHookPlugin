#include "HttpHook.h"
#include "ui_HttpHook.h"
#include "OpenRGBHttpHookPlugin.h"
#include "HookActions.h"
#include "ProfileManager.h"

#include <QMenu>
#include <QAction>
#include <QMainWindow>

HttpHook::HttpHook(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HttpHook)
{
    ui->setupUi(this);

    GenerateID();

    ui->verb->addItems({"POST", "GET"});

    ui->edit_hook_action->hide();
}

HttpHook::~HttpHook()
{
    delete ui;
}

void HttpHook::Rename(std::string value)
{
    name = value;
}

std::string HttpHook::GetName()
{
    return name;
}

void HttpHook::Run()
{
    printf("Running Hook %s \n", path.c_str());

    switch (action) {
        case HOOK_ACTION_LOAD_PROFILE:
        {
            std::string profile = sub_action;

            printf("Scheduled task run: load profile [%s]\n", profile.c_str());

            OpenRGBHttpHookPlugin::RMPointer->GetProfileManager()->LoadProfile(profile);

            for(RGBController* controller : OpenRGBHttpHookPlugin::RMPointer->GetRGBControllers())
            {
                controller->DeviceUpdateMode();

                if(controller->modes[controller->active_mode].color_mode == MODE_COLORS_PER_LED)
                {
                    controller->DeviceUpdateLEDs();
                }
            }

            break;
        }
        case HOOK_ACTION_TURN_OFF:
        {
            printf("Scheduled task run: turn off devices\n");

            for(RGBController* controller : OpenRGBHttpHookPlugin::RMPointer->GetRGBControllers())
            {
                controller->SetAllLEDs(ToRGBColor(0,0,0));
                controller->UpdateLEDs();
            }
            break;
        }
        case HOOK_ACTION_EFFECT_PLUGIN:
        {
            QString object_name = QString::fromStdString(sub_action);

            printf("Scheduled task run: effects plugin action: %s\n", object_name.toStdString().c_str());

            bool found = false;

            for (QWidget *w : QApplication::topLevelWidgets())
            {
                if (QMainWindow* mainWin = qobject_cast<QMainWindow*>(w))
                {
                    QAction* action = mainWin->findChild<QAction*>(object_name);

                    if(action)
                    {
                        found = true;
                        action->trigger();
                    }
                }
            }

            if(!found) printf("The action has not been found \n");
            else printf("The action was triggered\n");

        }
        default: break;
    }
}

void HttpHook::on_path_textChanged(const QString)
{
    path = ui->path->text().toStdString();

    if(path.empty())
    {
        GenerateID();
    }
}

void HttpHook::GenerateID()
{
    ui->path->setText(QString::fromStdString(std::to_string(rand())));
}

void HttpHook::on_edit_hook_action_updated(int action_idx, std::string sub_action_value)
{
    action = action_idx;
    sub_action = sub_action_value;
    ui->task->setText(HOOK_ACTIONS[action] +  " " + QString::fromStdString(sub_action));
}

void HttpHook::on_edit_clicked()
{
    ui->edit_hook_action->setVisible(!ui->edit_hook_action->isVisible());

    if(ui->edit_hook_action->isVisible())
    {
        ui->edit_hook_action->Reload();
    }
}

void HttpHook::on_verb_currentIndexChanged(int)
{
    verb = ui->verb->currentText().toStdString();
}

json HttpHook::ToJson()
{
    json j;

    j["verb"]       = ui->verb->currentIndex();
    j["path"]       = ui->path->text().toStdString();
    j["action"]     = action;
    j["name"]       = name;

    switch (action) {
    case HOOK_ACTION_LOAD_PROFILE:
    case HOOK_ACTION_EFFECT_PLUGIN:
    {
        j["sub_action"] = sub_action;
        break;
    }
    default: break;
    }

    return j;
}

void HttpHook::FromJSON(json j)
{
    if(j.contains("verb"))
    {
        ui->verb->setCurrentIndex(j["verb"]);
    }

    if(j.contains("path"))
    {
        ui->path->setText(QString::fromStdString(j["path"]));
    }

    if(j.contains("action"))
    {
        action = j["action"];;
    }

    if(j.contains("sub_action"))
    {
        sub_action = j["sub_action"];
    }

    if(j.contains("name"))
    {
        name = j["name"];
        emit Renamed(name);
    }

    ui->task->setText(HOOK_ACTIONS[action] +  " " + QString::fromStdString(sub_action));
}
