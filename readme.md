# Http Hooks Plugin

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/commits/master)

## What is this?

This is a plugin for [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) that allows you to set incoming webhooks. You can link an action (set profile, turn LEDs off) to a webhook.

## Experimental (Master)

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/artifacts/master/download?job=Windows%2032)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/artifacts/master/download?job=Windows%2064)
* [Linux 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/artifacts/master/download?job=Linux%2032)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/artifacts/master/download?job=Linux%2064)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/artifacts/master/download?job=MacOS%20ARM64)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/artifacts/master/download?job=MacOS%20Intel)

## Stable 0.9

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/4631712438/artifacts/download)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/4631712439/artifacts/download)
* [Linux 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/4631712435/artifacts/download)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/4631712437/artifacts/download)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/4631712442/artifacts/download)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/4631712443/artifacts/download)

## Stable (0.8)

* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/3418208256/artifacts/download)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/3418208254/artifacts/download)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/3418208257/artifacts/download)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBHttpHookPlugin/-/jobs/3418208258/artifacts/download)

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button
