#include "EditHookAction.h"
#include "ui_EditHookAction.h"
#include "HookActions.h"
#include "OpenRGBHttpHookPlugin.h"
#include "ProfileManager.h"

#include <QMainWindow>

EditHookAction::EditHookAction(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditHookAction)
{
    ui->setupUi(this);

    ui->action->addItems(HOOK_ACTIONS);
}

EditHookAction::~EditHookAction()
{
    delete ui;
}

void EditHookAction::Reload()
{
    ui->sub_action->blockSignals(true);

    ui->sub_action->clear();

    int action_index = ui->action->currentIndex();

    switch (action_index) {
        case HOOK_ACTION_LOAD_PROFILE:
        {
            std::vector<std::string> profiles = OpenRGBHttpHookPlugin::RMPointer->GetProfileManager()->profile_list;

            for(std::string& profile: profiles)
            {
                ui->sub_action->addItem(QString::fromStdString(profile), QString::fromStdString(profile));
            }

            break;
        }
        case HOOK_ACTION_EFFECT_PLUGIN:
        {
            QRegularExpression action_filter("^OpenRGBEffectsPlugin::Action::");

            for (QWidget *w : QApplication::topLevelWidgets())
            {
                if (QMainWindow* mainWin = qobject_cast<QMainWindow*>(w))
                {
                    QList<QAction*> profilesActions = mainWin->findChildren<QAction*>(action_filter);

                    for(const QAction* action : profilesActions)
                    {
                        ui->sub_action->addItem(action->property("OpenRGBEffectsPlugin::ActionTitle").toString(), action->objectName());
                    }
                }
            }
            break;
        }
        default: break;
    }

    ui->sub_action->blockSignals(false);
}

void EditHookAction::on_action_currentIndexChanged(int)
{
    Reload();
    emit updated(ui->action->currentIndex(), ui->sub_action->currentData().toString().toStdString());
}

void EditHookAction::on_sub_action_currentIndexChanged(int)
{
    emit updated(ui->action->currentIndex(), ui->sub_action->currentData().toString().toStdString());
}

